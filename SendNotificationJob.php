<?php

namespace console\jobs;

use Carbon\Carbon;
use common\components\notification\NotificationInterface;
use common\queue\AbstractJob;
use Kayex\HttpCodes;
use Yii;

/**
 * Module: Jobs
 *
 * Class SendNotificationJob
 * @package console\jobs
 */
class SendNotificationJob extends AbstractJob //odoslanie push notifikacie cez Google Firebase Messaging
{
    /**
     * @var NotificationInterface
     */
    private $notification;

    public function __construct(NotificationInterface $notification)
    {
        $this->notification = $notification;
    }

    /**
     * @inheritdoc
     */
    public function run(): bool //metoda run sa vola ked sa job vyberie z queue a ide spracovavat
    {
        $notification = $this->notification;

        printf("Sending notification (%s)", get_class($notification));

        $data = array_merge([
            'title' => $notification->getTitle(),
            'text' => $notification->getText(),
            'type' => $notification->getType(),
            'id' => $notification->getExternalId(),
            'sent_at' => (string)Carbon::now(),
            'valid_to' => $notification->getLifetime() == 0 ? null : (string)Carbon::now()->addSeconds($notification->getLifetime()),
        ], $notification->getData());

        $message = Yii::$app->fcm->createMessage($notification->getDevices())->setData($data);

        return (!$notification->isStored() || $notification->save())
            && (HttpCodes::HTTP_OK == Yii::$app->fcm->send($message)->getStatusCode());
    }
}