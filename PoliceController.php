<?php

namespace backend\controllers;

use common\behaviors\HttpAuth;
use common\components\Point;
use common\facades\Response;
use common\models\PolicePatrol;
use common\components\Settings;
use Kayex\HttpCodes;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UnprocessableEntityHttpException;

/**
 * Module: Police
 *
 * Class PoliceController
 * @package backend\controllers
 */
class PoliceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() //urcuje co sa ma vykonat pri udalostiach, ktore nastanu v objekte (pred spustenim akcie a pod)
    {
        return array_merge(parent::behaviors(), [
            'bearerAuth' => [
                'class' => HttpAuth::className(), //HTTP Bearer autorizacia
            ],
        ]);
    }

    /**
     * UC: UC57
     * Create new police patrol on given location
     *
     * @return array|PolicePatrol
     * @throws ServerErrorHttpException
     * @throws UnprocessableEntityHttpException
     */
    public function actionCreate()
    {
        $model = new PolicePatrol([
            'scenario' => PolicePatrol\Scenario::CREATE, //v Yii modely podporuju scenare, pri roznych scenaroch sa napr mozu validovat ine atributy modelu (napr. pri update mozeme jednoducho zakazat upravovanie nejakeho atributu)
        ]);

        if (!$this->loadModel($model)) {
            throw new UnprocessableEntityHttpException(); //Yii automaticky transformuje HTTP vynimky, v tomto pripade vrati status 422 s pripadnym vypisom chyb v jednotlivych atributoch
        }

        //check if there is a patrol nearby, and if is, update it
        $closest = PolicePatrol\Query::findInRadius(
        	$model->location,
        	Settings::get(Settings\Keys::POLICE_UPDATE_RADIUS)
        )->one();

        if ($closest != null) {
            if ($closest->markActive()) {
                Response::code(HttpCodes::HTTP_NOT_MODIFIED);

                return Response::message('Patrol was marked as still active');
            }

            throw new ServerErrorHttpException();
        }

        if ($model->save()) {
            Response::code(HttpCodes::HTTP_CREATED);
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException();
        }

        return $model; //model sa automaticky serializuje (xml/json) pripadne ak ma nejake chyby, tak sa vratia tie vo forme pola [atribut => [chyby...]]
    }

    /**
     * UC: UC58
     * Mark existing patrol as still active
     *
     * @param string $id UUID of selected patrol
     *
     * @return array|PolicePatrol
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->markActive()) {
            return $model;
        }

        throw new ServerErrorHttpException();
    }

    /**
     * UC: UC56
     * Find police patrols on given map projection
     *
     * @param string $northeast Upper-right coordinate of viewport
     * @param string $southwest Bottom-left coordinate of viewport
     * @return array|PolicePatrol[]
     * @throws UnprocessableEntityHttpException
     */
    public function actionInBounds($northeast, $southwest)
    {
        $northeast = Point::parse($northeast);
        $southwest = Point::parse($southwest);

        if (!Point::validateMultiple([$northeast, $southwest])) {
            throw new UnprocessableEntityHttpException('Invalid coordinates');
        }

        return PolicePatrol\Query::findInBounds($northeast, $southwest);
    }

    /**
     * UC: UC59
     * Mark existing patrol as no longer active
     *
     * @param string $id UUID of selected patrol
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->markInactive()) {
            Response::code(HttpCodes::HTTP_NO_CONTENT);

            return;
        }

        throw new ServerErrorHttpException();
    }

    /**
     * Finds police patrol by UUID or generates HTTP Not Found status
     *
     * @param string $id UUID to search for
     * @return PolicePatrol
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = PolicePatrol::find()->uuid($id)->activeOrTimedOut()->noRegular()->one();

        if ($model == null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}