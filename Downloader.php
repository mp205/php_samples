<?php

namespace app\components;

use Yii;
use app\models\Stat;
use app\models\Artist;
use app\models\Song;
use app\models\Radio;
use app\models\Stream;
use app\models\Song\RelSongArtist;
use app\models\Artist\Name as ArtistName;
use app\models\Song\Name as SongName;
use app\helpers;
use app\exceptions\NotFoundException;
use app\exceptions\ErrorException;
use \yii\base\NotSupportedException;

//podarilo sa mi najst projekt, ktory som spominal, tu je kus kodu. z tejto classy boli extendovane downloadery pre jednotlive radia/streamy
abstract class Downloader extends \yii\base\Component
{
    const CACHE_TIME = 180;

    protected $artist;
    protected $song;
    protected $time;
    protected $stream;

    protected $radio;

    /**
     * Get id of radio the downloader belongs to
     *
     * @return string
     */
    abstract public static function radioID();

    /**
     * Get and parse data from stream
     *
     * @param Stream $stream
     * @return mixed
     */
    abstract protected function getStream($stream);

    public function init()
    {
        $this->radio = Radio::findByAlias($this->radioID());

        if ($this->radio == null) {
            throw new NotFoundException(sprintf('The radio %s does not exist!', $this->radioID()));
        }
    }

    //TODO: better solution
    /**
     * Get playlist data from stream
     *
     * @param string $url Stream data url
     * @param bool $json Is playlist json encoded?
     * @return mixed
     *
     * @throws ErrorException
     */
    protected function makeRequest($url, $json = true)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, '');

        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        switch ($status) {
            case 200:
            case 201:
            case 202:
                return $json ? json_decode($result) : $result;

            default:
                throw new ErrorException($status, sprintf('Url: %s Status: %s Result: %s', $url, $status, $result));
        }
    }

    /**
     * @return Stream
     *
     * @throws NotFoundException
     */
    protected function prepareStream()
    {
        $stream = $this->radio->streams[$this->stream];

        if ($stream == null) {
            throw new NotFoundException(sprintf('The stream %s of radio %s does not exist', $this->stream,
                $this->radioID()));
        }

        return $stream;
    }

    /**
     * Finds or creates Artist by name
     * 
     * @return Artist
     * 
     * @throws NotSupportedException
     */
    protected function prepareArtist()
    {
        if (strlen($this->artist) == 0) {
            throw new NotSupportedException('Artist empty on stream ' . $this->stream);
        }

        $artist = Artist::findByName($this->artist);

        if ($artist == null) {
            $artistName = ArtistName::findByName($this->artist);

            if ($artistName == null || ($artist = $artistName->artist) == null) {
                return Artist::create($this->artist);
            }
        } elseif (!$artist->visible) {
            throw new NotSupportedException(sprintf('Artist %s is not visible, thus no stats are saved for it.',
                $this->artist));
        }

        return $artist;
    }

    /**
     * Finds or creates Song by name and its artist
     * 
     * @param Artist $artist
     * @return Song
     * 
     * @throws NotSupportedException
     */
    protected function prepareSong($artist)
    {
        if (strlen($this->song) == 0) {
            throw new NotSupportedException(sprintf('Song empty on stream %s with artist %s', $this->stream,
                $this->artist));
        }

        $song = Song::findByNameAndArtist($this->song, $artist->id);

        if ($song == null) {
            $songName = SongName::findByName($this->song);

            if ($songName == null || ($song = $songName->song) == null) {
                return Song::create($this->song, $artist);
            }
        } elseif (!$song->visible) {
            throw new NotSupportedException(sprintf('Song %s - %s is not visible, thus no stats are saved for it.',
                $this->artist, $this->song));
        }

        return $song;
    }

    protected function setCache($cache, $key)
    {
        $data = [
            'artist' => $this->artist,
            'song' => $this->song,
            'time' => $this->time,
            'stream' => $this->stream,
        ];

        $cache->set($key, $data, self::CACHE_TIME);
    }

    public function save()
    {
        $this->artist = trim($this->artist);
        $this->song = trim($this->song);

        if (strlen($this->artist) == 0) {
            list($this->artist, $this->song) = $this->parsePlainText($this->song);
        }

        echo sprintf('Stream: %s Artist: %s Song: %s<br>', $this->stream, $this->artist, $this->song);

        $cache = Yii::$app->cache;
        $key = sprintf('last_song_%s_%s', $this->radioID(), $this->stream);

        if ($cache->exists($key)) {
            $data = $cache[$key];

            if ($data['artist'] == $this->artist && $data['song'] == $this->song && $data['stream'] == $this->stream) {
                $this->setCache($cache, $key);

                return false;
            }
        }

        $this->setCache($cache, $key);

        $stream = $this->prepareStream();
        $artist = $this->prepareArtist();
        $song = $this->prepareSong($artist);

        $attrs = [
            'song_id' => $song->id,
            'radio_id' => $this->radio->id,
            'stream_id' => $stream->id,
            'year' => date('Y', $this->time),
            'month' => date('m', $this->time),
            'day' => date('d', $this->time),
            'hour' => date('H', $this->time),
            'minute' => date('i', $this->time),
        ];

        $stat = new Stat($attrs);

        return $stat->save();
    }

    public function download()
    {
        foreach ($this->radio->downloadableStreams as $stream) {
            try {
                if ($this->getStream($stream) !== false) {
                    $this->artist = StringHelper::replace('&nbsp;', ' ', $this->artist);
                    $this->song = StringHelper::replace('&nbsp;', ' ', $this->song);

                    $this->save();
                }
            } catch (NotFoundException | NotSupportedException $e) {
                Yii::info($e->getMessage());
            } catch (\Exception $e) {
                Yii::$app->notifier->notify(sprintf("File: %s\r\nLine: %s\r\nMessage: %s", $e->getFile(), $e->getLine(),
                    $e->getMessage()));

                Yii::warning($e->getMessage());
            }
        }
    }

    //TODO: better solution (regex or something?)
    /**
     * Split string to artist and song
     * 
     * @param string $data
     * @return array
     */
    protected function parsePlainText($data)
    {
        $data = StringHelper::replace('&nbsp;', ' ', $data); //helper metoda, nahrada za neexistujuci mb_str_replace

        $song = explode('-', $data, 2);

        return [
            'artist' => trim($song[0] ?? ''),
            'song' => trim($song[1] ?? ''),
        ];
    }
}