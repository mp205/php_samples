<?php

namespace backend\tests\functional;

use backend\tests\ApiTester;
use backend\tests\Cest;
use Codeception\Util\HttpCode;
use common\fixtures\PolicePatrolFixture;
use common\fixtures\UserFixture;
use common\models\PolicePatrol;
use common\models\PolicePatrol\Status;
use common\models\PolicePatrol\Type;

/**
 * Module: Test/Police
 *
 * Class PoliceCest
 * @package backend\tests\functional
 */
class PoliceCest extends Cest //functional testovanie endpointov v PoliceControlleri (metody nie su komentovane, v tomto pripade su to jednoduche testy)
{
    private $policeJsonType = [
        'uuid' => 'string',
        'location' => [
            'latitude' => 'float|integer',
            'longitude' => 'float|integer',
        ],
        'type' => 'string',
        'status' => 'string',
        'valid_to' => 'string',
        'created_at' => 'string',
        'last_status_change' => 'string',
    ];

    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'users.php',
            ],
            'policePatrol' => [
                'class' => PolicePatrolFixture::className(),
                'dataFile' => codecept_data_dir() . 'policePatrols.php',
            ],
        ];
    }

    public function createPolicePatrol(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $data = [
            'type' => Type::getName(Type::RADAR),
            'location' => [
                'latitude' => rand(-90, 90),
                'longitude' => rand(-180, 180),
            ],
        ];

        $i->wantTo('create police patrol');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendPOST('/police', $data);

        $i->seeResponseCodeIs(HttpCode::CREATED);
        $i->seeResponseIsJson();
        $i->seeResponseContainsJson($data);
        $i->seeResponseMatchesJsonType($this->policeJsonType);
    }

    public function createRegularPolicePatrol(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $data = [
            'type' => Type::getName(Type::REGULAR), //Regular su pravidelne (X krat sa objavi v nejakom radiuse hliadka), tie vyhladava cron job, nedaju sa vytvorit
            'location' => [
                'latitude' => rand(-90, 90),
                'longitude' => rand(-180, 180),
            ],
        ];

        $i->wantTo('create regular police patrol');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendPOST('/police', $data);

        $i->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY);
    }

    public function createPolicePatrolNearby(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabActive($i);

        $data = [
            'location' => $patrol->location,
        ];

        $i->wantTo('create police patrol near one existing');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendPOST('/police', $data);

        $i->seeResponseCodeIs(HttpCode::NOT_MODIFIED);
    }

    public function createPolicePatrolNearbyInactive(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabInactive($i);

        $data = [
            'type' => Type::getName(Type::RADAR),
            'location' => $patrol->location->toArray(),
        ];

        $i->wantTo('create police patrol near one existing but inactive');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendPOST('/police', $data);

        $i->seeResponseCodeIs(HttpCode::CREATED);
        $i->seeResponseIsJson();
        $i->seeResponseContainsJson($data);
        $i->seeResponseMatchesJsonType($this->policeJsonType);
    }

    public function updatePolicePatrol(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabActive($i);

        $i->wantTo('update police patrol');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendPUT('/police/' . $patrol->uuid);

        $i->seeResponseCodeIs(HttpCode::OK);
        $i->seeResponseIsJson();
        $i->seeResponseMatchesJsonType($this->policeJsonType);
    }

    public function updateInactivePolicePatrol(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabInactive($i);

        $i->wantTo('update inactive police patrol');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendPUT('/police/' . $patrol->uuid);

        $i->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function updateRegularPolicePatrol(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabRegular($i);

        $i->wantTo('update regular police patrol');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendPUT('/police/' . $patrol->uuid);

        $i->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function deletePolicePatrol(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabActive($i);

        $i->wantTo('delete police patrol');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendDELETE('/police/' . $patrol->uuid);

        $i->seeResponseCodeIs(HttpCode::NO_CONTENT);
    }

    public function deleteInactivePolicePatrol(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabInactive($i);

        $i->wantTo('delete inactive police patrol');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendDELETE('/police/' . $patrol->uuid);

        $i->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function deleteRegularPolicePatrol(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabRegular($i);

        $i->wantTo('delete regular police patrol');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendDELETE('/police/' . $patrol->uuid);

        $i->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function findInBounds(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabActive($i);

        $lat = $patrol->location->latitude;
        $lng = $patrol->location->longitude;

        $ne = ($lat + .01) . ',' . ($lng + .01);
        $sw = ($lat - .01) . ',' . ($lng - .01);

        $i->wantTo('find patrols in bounds');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendGET("/police/in-bounds?northeast={$ne}&southwest={$sw}");

        $i->seeResponseCodeIs(HttpCode::OK);
        $i->seeResponseIsJson();
        $i->seeResponseContainsJson($patrol->toArray());
        $i->seeResponseMatchesJsonType($this->policeJsonType, '$.[0]');
    }

    public function findOutOfBounds(ApiTester $i)
    {
        $this->loginUser($i, 1);

        $patrol = $this->grabActive($i);

        $lat = $patrol->location->latitude;
        $lng = $patrol->location->longitude;

        $ne = ($lat + 1) . ',' . ($lng + 1);
        $sw = ($lat + .01) . ',' . ($lng + .01);

        $i->wantTo('find patrols in bounds');
        $i->amBearerAuthenticated($this->accessToken);
        $i->sendGET("/police/in-bounds?northeast={$ne}&southwest={$sw}");

        $i->seeResponseCodeIs(HttpCode::OK);
        $i->seeResponseIsJson();
        $i->dontSeeResponseContainsJson($patrol->toArray());
    }

    private function grabActive(ApiTester $i)
    {
        return $i->grabRecord(PolicePatrol::className(),
            ['type' => [Type::RADAR, Type::CHECK], 'status' => [Status::ACTIVE, Status::TIMED_OUT]]);
    }

    private function grabInactive(ApiTester $i)
    {
        return $i->grabRecord(PolicePatrol::className(),
            ['type' => [Type::RADAR, Type::CHECK], 'status' => Status::INACTIVE]);
    }

    private function grabRegular(ApiTester $i)
    {
        return $i->grabRecord(PolicePatrol::className(), ['type' => Type::REGULAR, 'status' => Status::ACTIVE]);
    }
}